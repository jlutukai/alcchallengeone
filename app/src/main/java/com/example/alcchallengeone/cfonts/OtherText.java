package com.example.alcchallengeone.cfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class OtherText extends androidx.appcompat.widget.AppCompatTextView {

public OtherText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        }

public OtherText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        }

public OtherText(Context context) {
        super(context);
        init();
        }

private void init() {
        if (!isInEditMode()) {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Champagne & Limousines Bold Italic.ttf");
        setTypeface(tf);
        }
        }

        }