package com.example.alcchallengeone.cfonts;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class Headingtext extends androidx.appcompat.widget.AppCompatTextView {

    public Headingtext(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public Headingtext(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public Headingtext(Context context) {
        super(context);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/Comfortaa-Bold.ttf");
            setTypeface(tf);
        }
    }

}