package com.example.alcchallengeone;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import java.net.URL;

import spencerstudios.com.bungeelib.Bungee;

import static android.view.View.LAYER_TYPE_HARDWARE;
import static android.webkit.WebView.RENDERER_PRIORITY_BOUND;

public class About extends AppCompatActivity implements View.OnClickListener {
    private static final String myurl = "https://andela.com/alc/";
    private ImageView back;
    Activity activity;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);



       WebView webView = findViewById(R.id.webview);
       back = findViewById(R.id.back_about);
       back.setOnClickListener(this);


        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);


        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error){
                handler.proceed();
            }

        });
        webView.loadUrl(myurl);
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(About.this, MainActivity.class));
        Bungee.slideLeft(this);
        finish();
    }
}
