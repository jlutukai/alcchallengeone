package com.example.alcchallengeone;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

import spencerstudios.com.bungeelib.Bungee;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView button1, button2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         button1 = findViewById(R.id.button1);
         button2 = findViewById(R.id.button2);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Bungee.split(this);
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v == button1){
            startActivity(new Intent(MainActivity.this, About.class));
            Bungee.slideRight(this);
        }
        if (v == button2){
            startActivity(new Intent(MainActivity.this, Profile.class));
            Bungee.slideLeft(this);
        }
    }
}
